﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GoldenTax
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void ToolStripMenuItemExport_Click(object sender, EventArgs e)
        {
            FormExport mr = null;
            foreach (Form ftmp in this.MdiChildren)
            {
                if (ftmp is FormExport)
                {
                    mr = (FormExport)(ftmp);
                    break;
                }
            }
            if (mr == null || mr.Disposing)
            {
                mr = new FormExport();
                mr.MdiParent = FormExport.ActiveForm;
                mr.WindowState = FormWindowState.Maximized;
            }
            this.ActivateMdiChild(mr);
            mr.WindowState = FormWindowState.Maximized;
            mr.Show();
            mr.Activate();
        }

        private void ToolStripMenuItemImport_Click(object sender, EventArgs e)
        {
            FormImport mr = null;
            foreach (Form ftmp in this.MdiChildren)
            {
                if (ftmp is FormImport)
                {
                    mr = (FormImport)(ftmp);
                    break;
                }
            }
            if (mr == null || mr.Disposing)
            {
                mr = new FormImport();
                mr.MdiParent = FormImport.ActiveForm;
                mr.WindowState = FormWindowState.Maximized;
            }
            this.ActivateMdiChild(mr);
            mr.WindowState = FormWindowState.Maximized;
            mr.Show();
            mr.Activate();
        }

        private void ToolStripMenuItemAbout_Click(object sender, EventArgs e)
        {
            FormAbout mr = null;
            foreach (Form ftmp in this.MdiChildren)
            {
                if (ftmp is FormAbout)
                {
                    mr = (FormAbout)(ftmp);
                    break;
                }
            }
            if (mr == null || mr.Disposing)
            {
                mr = new FormAbout();
                mr.MdiParent = FormAbout.ActiveForm;
                mr.WindowState = FormWindowState.Maximized;
            }
            this.ActivateMdiChild(mr);
            mr.WindowState = FormWindowState.Maximized;
            mr.Show();
            mr.Activate();
        }

        private void menuMain_ItemAdded(object sender, ToolStripItemEventArgs e)
        {
            if (e.Item.Text.Length == 0 //隐藏子窗体图标
                || e.Item.Text == "最小化(&N)" //隐藏最小化按钮
                || e.Item.Text == "还原(&R)" //隐藏还原按钮
                //|| e.Item.Text == "关闭(&C)"
                ) //隐藏最关闭按钮
            {
                e.Item.Visible = false;
            }
        }
    }
}
